from shapes import *

def triangle(r):
	s = Circle(r).translate(0,r/2)
	s &= Circle(r).translate(0,r/2).rotate(1/3)
	s &= Circle(r).translate(0,r/2).rotate(2/3)
	s -= Circle(r/2 - 0.001)
	return s

for i in range(3):
	shape += triangle(3*(3/4)**i).rotate(1/6*i)

shape = shape.translate(0,-0.2)