import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import cairo
from interval import *
from threading import Thread
from queue import Queue
from time import sleep, time

class RenderPane(Gtk.Image):

    def __init__(self, area, expression, resolution):
        self.area = area
        self.expression = expression
        self.resolution = resolution

        Gtk.DrawingArea.__init__(self)

        self.surface = cairo.ImageSurface(cairo.Format.RGB24, 800, 800)
        self.set_from_surface(self.surface)
        self.context = cairo.Context(self.surface)
        self.context.translate(400, 400)
        self.context.scale(200, 200)
        self.context.set_antialias(cairo.Antialias.NONE)

        Thread(target = self.update_surface_thread, daemon = True).start()
        self.render()

    def render(self, widget = None):
        Thread(target = self.render_thread, daemon = True).start()

    def render_thread(self):
        self.context.set_source_rgb(0.5,0.5,0.5)
        self.context.paint()
        start_time = time()

        queue = Queue()
        queue.put(self.area)

        def worker():
            while not queue.empty():
                x0,y0,x1,y1 = queue.get()
                xm = (x0 + x1)/2.0
                ym = (y0 + y1)/2.0
                if abs(x0 - x1) < self.resolution:
                    self.draw_rectangle(x0,y0,x1,y1, self.expression(xm,ym))
                    continue
                result = self.expression(Interval(x0, x1), Interval(y0, y1))
                if isinstance(result, bool):
                    self.draw_rectangle(x0,y0,x1,y1, result)
                    continue
                queue.put((x0,y0,xm,ym))
                queue.put((xm,y0,x1,ym))
                queue.put((x0,ym,xm,y1))
                queue.put((xm,ym,x1,y1))

        worker()
        print("FINISHED RENDERING in", time() - start_time, "seconds")

    def draw_rectangle(self, xa,ya,xb,yb, b):
        #color = (0, random(), 1) if b else (1, random(), 0)
        color = (1, 1, 1) if b else (0, 0, 0)
        d = abs(xa - xb)
        self.context.set_source_rgb(*color)
        self.context.rectangle(xa, ya, d, d)
        self.context.fill()

    def update_surface_thread(self):
        while True:
            self.queue_draw()
            sleep(1/10.0)
